# This Python file uses the following encoding: utf-8
import os,sys
import PyQt5
from PyQt5.QtGui import QImage

def hist(img: QImage):
    ret = [[],[],[]]
    for m in ret:
        for i in range(256):
            m.append(0)
    for i in range(img.width()):
        for j in range(img.height()):
            color = img.pixelColor(i,j)
            ret[0][color.red()] += 1
            ret[1][color.green()] += 1
            ret[2][color.blue()] += 1
    return ret

def makeHalftone(img: QImage):
    pass

def load_image(file_name):
    img = QImage()
    if not img.load(file_name):
        raise FileNotFoundError(file_name)
    return img
