import PyQt5
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QWidget,\
    QHBoxLayout, QPushButton,\
    QApplication, QLabel
from .ImageView import ImageView


def main():
    app = QApplication.instance()
    if app is None:
        app = QApplication([])
    widget = QWidget()
    img = QImage()
    img.load("neo-matrix.jpg")
    v = ImageView()
    v.showImage(img)
    v.clicked.connect(lambda : print("yay"))
    layout = QHBoxLayout()
    layout.addWidget(v)
    widget.setLayout(layout)
    widget.show()
    app.exec_()
