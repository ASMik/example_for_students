# This Python file uses the following encoding: utf-8
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from .image_functions import load_image

class ImageView(QtWidgets.QScrollArea):
    def __init__(self):
        QtWidgets.QScrollArea.__init__(self)
        self.label = QtWidgets.QLabel(
            self)
        self.label.setSizePolicy(
            QtWidgets.QSizePolicy.Expanding,
            QtWidgets.QSizePolicy.Expanding)
        self.setBackgroundRole(
            QtGui.QPalette.Dark)
        self.setWidget(self.label)
        self.setWidgetResizable(True)
        self.installEventFilter(self)
    
    def showImage(self, img:QtGui.QImage):
        self.label.setPixmap(
            QtGui.QPixmap().fromImage(
                img))
    def showDifferenceImage(self, dImg):
        QPixMap(len(dImg),...)
        # ...
        pass

    clicked = QtCore.pyqtSignal()
    def eventFilter(self, obj, event):
        if obj == self:
            if event.type() == QtCore.QEvent.MouseButtonRelease:
                if obj.rect().contains(event.pos()):
                    self.clicked.emit()
                    return True
        return False
